package handlers

import (
	"context"
	"errors"
	"net/http"
	"time"

	echo "github.com/labstack/echo/v4"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func newContext() context.Context {
	c, _ := context.WithTimeout(context.Background(), 30*time.Second)
	return c
}

type Cat struct {
	ID    primitive.ObjectID `bson:"_id,omitempty"`
	Name  string
	Color string
	Owner string
}

func GetCat(mdb *mongo.Client) echo.HandlerFunc {
	return func(c echo.Context) error {
		mctx := newContext()

		idParam := c.Param("id")
		if idParam == "" {
			return errors.New("ID cannot be empty")
		}
		id, err := primitive.ObjectIDFromHex(idParam)
		if err != nil {
			return errors.New("Invalid ID")
		}

		var cat Cat

		err = mdb.Database("catabase").Collection("cats").FindOne(mctx, bson.D{{"_id", id}}).Decode(&cat)
		if err == mongo.ErrNoDocuments {
			return echo.NewHTTPError(404, "Cat not found")
		} else if err != nil {
			return err
		}

		return c.JSON(http.StatusOK, cat)
	}
}

func GetAllCats(mdb *mongo.Client) echo.HandlerFunc {
	return func(c echo.Context) error {
		mctx := newContext()

		cats := new([]Cat)

		cur, err := mdb.Database("catabase").Collection("cats").Find(mctx, bson.D{})
		if err != nil {
			return err
		}

		if err := cur.All(mctx, cats); err != nil {
			return err
		}

		return c.JSON(http.StatusOK, cats)
	}
}

func PostCat(mdb *mongo.Client) echo.HandlerFunc {
	return func(c echo.Context) error {
		mctx := newContext()

		cat := new(Cat)
		err := c.Bind(cat)
		if err != nil {
			return err
		}

		b, err := bson.Marshal(cat)

		res, err := mdb.Database("catabase").Collection("cats").InsertOne(mctx, bson.Raw(b))
		if err != nil {
			return err
		}

		cat.ID = res.InsertedID.(primitive.ObjectID)

		return c.JSON(http.StatusOK, cat)
	}
}

func PutCat(mdb *mongo.Client) echo.HandlerFunc {
	return func(c echo.Context) error {
		mctx := newContext()

		idParam := c.Param("id")
		if idParam == "" {
			return errors.New("ID cannot be empty")
		}
		id, err := primitive.ObjectIDFromHex(idParam)
		if err != nil {
			return errors.New("Invalid ID")
		}

		cat := new(Cat)
		err = c.Bind(cat)
		if err != nil {
			return err
		}
		cat.ID = id

		b, err := bson.Marshal(cat)
		if err != nil {
			return err
		}

		res, err := mdb.Database("catabase").Collection("cats").UpdateByID(mctx, id, bson.D{{"$set", bson.Raw(b)}})
		if err != nil {
			return err
		}

		if res.ModifiedCount > 0 {
			return c.JSON(http.StatusOK, cat)
		} else {
			return c.NoContent(http.StatusNotFound)
		}
	}
}

func DeleteCat(mdb *mongo.Client) echo.HandlerFunc {
	return func(c echo.Context) error {
		mctx := newContext()
		idParam := c.Param("id")
		if idParam == "" {
			return errors.New("ID cannot be empty")
		}
		id, err := primitive.ObjectIDFromHex(idParam)
		if err != nil {
			return errors.New("Invalid ID")
		}

		_, err = mdb.Database("catabase").Collection("cats").DeleteOne(mctx, bson.D{{"_id", id}})
		if err != nil {
			return err
		}

		return c.NoContent(http.StatusNoContent)
	}
}
